from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils.translation import gettext_lazy as _
from django.core.mail import send_mail
from django.utils import timezone

from restaurant.models import Address, Restaurant

class UserManager(BaseUserManager):
	"""
	Author: Daking Rai
	Date: 2018/06/10
	Description: UserManager class is copy of default User model provided by Django. No modification 
	is done in this class.
	"""

	def _create_user(self,username, email, password, **extra_fields):
		if not email:
			raise ValueError('User must have email address')

		email = self.normalize_email(email)
		address = Address.objects.create()
		user = self.model(username=username, email=email, address=address, **extra_fields)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, username, email=None, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(username, email, password, **extra_fields)

	def create_superuser(self, username, email, password, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')

		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')

		return self._create_user(username, email, password, **extra_fields)



class User(AbstractBaseUser, PermissionsMixin):
	"""
	Author: Daking Rai
	Date: 2018/06/10
	Description: User Class extends user model provided by django to add new column - email,address,
	is_restaurant_staff,website_staff. 
	AUTH_USER_MODEL is also added in settings.py to tell the program to include this models in user model.
	"""
	username = models.CharField(
		_('username'),
		max_length=150,
		blank=True,
		help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
		error_messages={
            'unique': _("A user with that username already exists."),
            },
            )
	email = models.EmailField(_('email address'), unique=True, null=True)
	address = models.ForeignKey(Address, on_delete = models.CASCADE, null=True,blank=True)

	is_restaurant_staff = models.BooleanField(default=False, help_text="Designates whether user is \
		restaurant staff of some restaurant")
	is_website_staff = models.BooleanField(default=False, help_text="Designates whether user is website staff.")

	is_staff = models.BooleanField(
		_('staff status'),
		default=False,
		help_text=_('Designates whether the user can log into this admin site.'),
		)
	is_active = models.BooleanField(
		_('active'),
		default=True,
		help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
            ),
		)
	date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

	objects = UserManager()

	EMAIL_FIELD = 'email'
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username']

	def get_full_name(self):
		full_name = '%s' % (self.username)
		return full_name.strip()

	def get_short_name(self):
		return self.email

	def email_user(self, subject, message, from_email=None, **kwargs):
		send_mail(subject, message, from_email, [self.email], **kwargs)

class RestaurantOwner(models.Model):
	"""
	Author: Daking Rai
	Date: 2018/06/13
	Description: This is model for restaurnt owner. User is manager if is_manager is true and 
	only staff if is_manager is false. One user can only be assciated with one restaurant.
	"""

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	is_manager = models.BooleanField(default=False)
	restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

	class Meta:
		db_table = 'restaurant_owner'
		verbose_name_plural = 'rest_owners'

	def __str__(self):
		return ('{}{}'.format(self.user,self.restaurant))
