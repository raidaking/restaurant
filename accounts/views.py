from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponseNotFound
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from accounts.models import User
from restaurant.models import Address
from accounts.forms import UserForm, LoginForm, UserProfileForm
from shopping.models import Order, OrderDetail
from restaurant.forms import AddressForm


# View function for registration of user
def create_user(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            address = Address.objects.create()
            user = form.save(commit=False)
            user.address = address
            form.save()

            messages.info(request, "created successfully!")
        else:
            messages.error(request, 'Something is Wrong!')
        return HttpResponseRedirect('/accounts/login/')
    else:
        form = UserForm()
        context_dict = {
            'forms': form,
        }

    return render(request,'create_user.html', context_dict)

# View function for login of user
def login_page(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(email=email,
                                password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                messages.error(request,'Invalid login credentials!')
                return render(request, 'login_page.html')
    else:
        form = LoginForm()
        return render(request,'login_page.html',{'forms':form})

# View function to handle logout
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login/')

def load_user_profile(request):
    if request.method == 'POST':
        user_form = UserProfileForm(request.POST)
        

        # if not request.user.address:
        #     address_form = AddressForm(request.POST)
        #     if user_form.is_valid() and address_form.is_valid():
        #         a = address_form.save()
        #         User.objects.filter(id=request.user.id).update(
        #             username= user_form.cleaned_data['username'],
        #             email = user_form.cleaned_data['email'],
        #             address = a,
        #         )
        # else:
        address_form = AddressForm(request.POST,instance = request.user.address)
        if user_form.is_valid() and address_form.is_valid():
            address_form.save()
            User.objects.filter(id=request.user.id).update(
                username=user_form.cleaned_data['username'],
                email=user_form.cleaned_data['email']
            )

        return HttpResponseRedirect('/profile')
    
    else:
        order= Order.objects.filter(user=request.user)

        order_details = OrderDetail.objects.filter(order__in=order)
        
        context_dict={
        'order_details': order_details,
        }
        return render(request,'user/profile.html',context_dict)



    