from django.contrib import admin

from accounts.models import RestaurantOwner,User

admin.site.register(User)
admin.site.register(RestaurantOwner)
