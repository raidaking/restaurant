from django.contrib import admin

from catalogs.models import Category, Food, Slider

# admin.site.register(Category)
# admin.site.register(Food)

from catalogs.forms import FoodAdminForm

class FoodAdmin(admin.ModelAdmin):
	form = FoodAdminForm
	list_display = ('name','price','created_at','updated_at',)
	list_display_links = ('name',)
	list_per_page = 30
	ordering = ['-created_at']
	search_fields = ['name','description', 'meta_keywords','meta_description']
	exclude = ['created_at','updated_at']
	prepopulated_fields = {'slug':('name','category')}

admin.site.register(Food,FoodAdmin)

class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name','created_at','updated_at')
	list_display_links = ('name',)
	list_per_page = 20
	ordering = ['name']
	search_fields = ['name','description','meta_keywords','meta_description']
	exclude = ['created_at','updated_at']
	prepopulated_fields = {'slug':('name',)}

admin.site.register(Category, CategoryAdmin)
admin.site.register(Slider)