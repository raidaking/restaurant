from django.db import models
from restaurant.models import Restaurant
from django.utils.text import slugify

class Category(models.Model):
	"""
	Date: June 12, 2018
	Author: Daking Rai
	Description: This is model for category of food.
	"""
	name = models.TextField(max_length=200)
	slug = models.SlugField(unique=True)
	description = models.TextField()
	parent = models.ForeignKey('self',blank=True, null=True ,related_name='children',on_delete=models.CASCADE)
	is_active = models.BooleanField(default=True)
	is_featured = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True,blank=True, null = True)
	image = models.ImageField(upload_to='category/%Y/%m/%d', null=True)

	class Meta:
		db_table = 'categories'
		ordering = ['-created_at']
		verbose_name_plural = 'Categories'
		unique_together = ('slug', 'parent',)     

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.name)
		super(Category, self).save(*args, **kwargs)

	def __str__(self):                          
		full_path = [self.name]                  
		k = self.parent                          

		while k is not None:
			full_path.append(k.name)
			k = k.parent

		return ' -> '.join(full_path[::-1])

	@models.permalink
	def get_absolute_url(self):
		return ('load_food_category', [self.name,])

class Food(models.Model):
	"""
	Date: June 12, 2018
	Author: Daking Rai
	Description: This is model for food.
	"""
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=1000, blank=True, null=True)
	price = models.FloatField(blank=False, null=False)
	image = models.ImageField(upload_to='food/%Y/%m/%d', null=True)
	slug = models.SlugField(unique=True)
	is_active = models.BooleanField(default=True)
	is_bestseller = models.BooleanField(default=False)
	is_featured = models.BooleanField(default=False)
	meta_keywords = models.CharField(max_length=255, help_text='Comma seperated keywords that is used for SEO', blank=True, null = True)
	meta_description = models.CharField(max_length=255, help_text='description of meta tags', blank=True, null = True)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, blank=True, null = True)
	category =  models.ManyToManyField(Category, blank=True)
	restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

	class Meta:
		db_table = 'Food'
		ordering = ['-created_at']

	def __str__(self):
		return ('%s'%self.name)

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.name,self.restaurant)
		super(Food, self).save(*args, **kwargs)

	@models.permalink
	def get_absolute_url(self):
		return ('load_food', [self.id,])


class Slider(models.Model):
	title = models.CharField(max_length=120)
	btn_description = models.CharField(max_length=120,default="Order Now")
	description = models.CharField(max_length=120)
	image = models.ImageField(upload_to='slider/%Y/%m/%d', null=True, blank=True)
	url = models.CharField(max_length=500, null=True)


	class Meta:
		db_table = 'Slider'
		#ordering = ['-created_at']

	def __str__(self):
		return ('%s'%self.name)

	def save(self, *args, **kwargs):
		super(Slider, self).save(*args, *kwargs)

	@models.permalink
	def get_absolute_url(self):
		return ('catalogs_slider', [self.slug,])
