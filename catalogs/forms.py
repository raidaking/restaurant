from django import forms
from catalogs.models import Food, Category, Slider

class FoodAdminForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = '__all__'

		def clean_price(self):
			if self.cleaned_data['price'] <= 0:
				raise forms.ValidationError('Price must be greater than zero.')
			return self.cleaned_data['price']

class FoodForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = '__all__'
		exclude = ['slug','created_at','updated_at']

class MenuForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = '__all__'
		exclude = ['slug','created_at','updated_at','restaurant']

class MenuEditForm(forms.ModelForm):
	restaurant = forms.CharField(widget=forms.HiddenInput())
	class Meta:
		model = Food
		fields = '__all__'

class CategoryForm(forms.ModelForm):

	class Meta:
		model = Category
		fields = '__all__'
		exclude = ['slug','created_at','updated_at']

class SliderForm(forms.ModelForm):
	class Meta:
		model = Slider
		fields = '__all__'
		