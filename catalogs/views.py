from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import user_passes_test
from django.views.decorators.csrf import csrf_exempt

from catalogs.models import Food, Category, Slider
from restaurant.models import Restaurant
from reviews.models import Comment
from catalogs.forms import FoodForm, CategoryForm, MenuForm, MenuEditForm, SliderForm
from accounts.models import RestaurantOwner
from shopping.models import Order, OrderDetail

def website_user(user):
    return user.groups.filter(name='website').exists()

def restaurant_user(user):
	return user.groups.filter(name='restaurant').exists()

def home(request):
	"""View function to load home page"""
	restaurants = Restaurant.objects.only('id','name', 'address')[:3]
	categories = Category.objects.filter(is_featured=True)[:6]
	sliders = Slider.objects.all()
	context_dict = {
		'restaurants': restaurants,
		'categories': categories,
		
		'sliders': sliders
		}
	return render(request,'home.html',context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def dashboard(request):
	"""View function to load dashboard for website owner"""
	orders = Order.objects.filter(user=request.user).order_by('-status', '-ordered_at')
	order_on_process = Order.objects.filter(user=request.user, status='ON_PROCESS').count()
	order_on_delivery = Order.objects.filter(user=request.user, status='ON_DELIVERY').count()
	order_delivered = Order.objects.filter(user=request.user, status='DELIVERED').count()

	context_dict = {
		'orders': orders,
		# 'order_details': order_details,
		'order_on_process': order_on_process,
		'order_on_delivery': order_on_delivery,
		'order_delivered': order_delivered,
	}

	return render(request, 'dashboard/dashboard.html',context_dict)

def not_found(request):
	return render(request,'404.html')

def load_food(request,id):
	"""View function to load single food"""
	food = get_object_or_404(Food,id=id)
	comments = Comment.objects.filter(food=id)
	context_dict = {
	'food': food,
	'comments':comments,
	}
	return render(request,'food/food.html',context_dict)

def load_error(request):
	return render(request,'404.html')

def load_food_category(request,name):
	"""
	View function to load food from certain category
	"""
	foods = Food.objects.filter(category__name=name)
	category = get_object_or_404(Category, name=name)
	context_dict = {
        'foods': foods,
        'category': category
    }
	return render(request,'food/foodcategory.html',context_dict)

# Views function for Admin panel

@login_required
@user_passes_test(website_user,login_url='/404')
def dashboard_food(request):
	foods = Food.objects.all()
	context_dict = {
		'foods': foods
	}
	return render(request, 'food-admin/food.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def add_food(request):
	if request.method == 'POST':
		form = FoodForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			messages.info(request, "Form was successfully saved!")
		else:
			messages.error(request, "Something was wrong!")
		return HttpResponseRedirect('/dashboard/food/')
	else:
		form = FoodForm()
		context_dict = {
			'form':form,
		}
		return render(request, 'food-admin/addfood.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def edit_food(request,id):
	if request.method=='POST':
		food = Food.objects.get(id=id)
		form = FoodForm(request.POST,request.FILES,instance=food)
		if form.is_valid():
			form.save()
			messages.info(request,"Food Information was successfully saved!")
		else:
			messages.error(request, "Sorry, something went wrong!")

		return HttpResponseRedirect('/dashboard/food/')
	else:
		food = Food.objects.get(id=id)
		form = FoodForm(instance=food)
		context_dict = {
		'form': form,
		}
		return render(request,'food-admin/editfood.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def delete_food(request,id):
	food = Food.objects.get(id=id)
	try:
		food.delete()
		messages.info(request, "It was successfully deleted!")
	except:
		messages.info(request, "Something is wrong!")

	return HttpResponseRedirect('/dashboard/food/')


# Views function for handling category model for food in backend
@login_required
@user_passes_test(website_user,login_url='/404')
def category(request):
	categories = Category.objects.all()
	context_dict = {
		'categories': categories
	}
	return render(request, 'category-admin/categories.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def add_category(request):
	if request.method == 'POST':
		form = CategoryForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			messages.info(request,"Category added!")
		else:
			messages.error(request,"Something is Wrong!")
		return HttpResponseRedirect('/dashboard/categories/')
	else:
		form = CategoryForm()
		context_dict = {
		'form': form
		}
		return render(request, 'category-admin/addcategories.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def edit_category(request,id):
	if request.method == 'POST':
		category = Category.objects.get(id=id)
		form = CategoryForm(request.POST, request.FILES, instance=category)
		if form.is_valid():
			form.save()
			messages.info(request,"Category edited successfully.")
		else:
			messages.error(request, "Something is Wrong!")
		return HttpResponseRedirect('/dashboard/categories/')
	else:
		category = Category.objects.get(id=id)
		form = CategoryForm(instance=category)
		context_dict = {
			'form': form,
		}
		return render(request, 'category-admin/editcategories.html', context_dict)

@login_required
@user_passes_test(website_user,login_url='/404')
def delete_category(request, id):
	category = Category.objects.get(id=id)
	try:
		category.delete()
		messages.info(request, "Deleted Successfully!")
	except:
		messages.error(request, "Something went wrong!")
	return HttpResponseRedirect('/dashboard/categories/')

@login_required
@user_passes_test(restaurant_user,login_url='/404')
def load_my_food(request):
	restaurant_owner = get_object_or_404(RestaurantOwner,user=request.user)
	menu = Food.objects.filter(restaurant= restaurant_owner.restaurant)
	restaurant = get_object_or_404(Restaurant,id=restaurant_owner.restaurant.id)
	context_data = {
		'menu': menu,
		'restaurant': restaurant,
	}
	return render(request, 'restaurant-owner/food.html',context_data)

@login_required
@user_passes_test(restaurant_user,login_url='/404')
def add_my_food(request):
	"""
	Author: Daking Rai
	Date: July 2, 2018
	Description: This is view function for adding food for specific restaurant for restaurant
	owner.The restaurant owner can only add the food of his restaurant.
	:param id: food id
	"""
	if request.method == 'POST':
		form = MenuForm(request.POST,request.FILES)
		if form.is_valid():
			restaurant_owner = get_object_or_404(RestaurantOwner, user=request.user)
			new_food = form.save(commit=False)
			new_food.restaurant = get_object_or_404(Restaurant, id=restaurant_owner.restaurant.id)
			new_food.save()
			categories = form.cleaned_data['category']
			for category in categories:
				c = get_object_or_404(Category, name=category)
				new_food.category.add(c)
				new_food.save()
			messages.info(request, "food added successfully!")
		else:
			messages.error(request, "Something is wrong, please try again!")
		return HttpResponseRedirect('/restaurant/dashboard/food/')
	else:
		form = MenuForm()
		context_dict = {
			'form': form,
		}
		return render(request, 'restaurant-owner/addfood.html',context_dict)

@login_required
@user_passes_test(restaurant_user,login_url='/404')
def edit_my_food(request,id):
	"""
	Author: Daking Rai
	Date: July 2, 2018
	Description: This is view function for editing food for specific restaurant for restaurant
	owner.The restaurant owner can only edit the food of his restaurant.
	:param id: food id
	"""
	restaurant_owner = get_object_or_404(RestaurantOwner, user=request.user)
	food = get_object_or_404(Food, id=id)
	if restaurant_owner.restaurant == food.restaurant:
		if request.method == 'POST':
			form = FoodForm(request.POST,request.FILES, instance=food)
			if form.is_valid():
				form.save()
				messages.info(request, "food Edited successfully!")
			else:
				messages.error(request, "Something is wrong!")

			return HttpResponseRedirect('/restaurant/dashboard/food/')

		else:
			form = MenuEditForm(instance=food)
			context_dict = {
				'form': form,
			}
			return render(request, 'restaurant-owner/editfood.html',context_dict)
	else:
		return HttpResponseRedirect('/404')


# Views for slider

@login_required
def dashboard_slider(request):
    sliders = Slider.objects.all()
    context_dict = {
        'sliders': sliders
	}
    return render(request, 'slider-admin/slider.html', context_dict)

@login_required
def add_slider(request):
    if request.method == 'POST':
        form = SliderForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/dashboard/slider/')
    else:
        form = SliderForm()
        context_dict = {'form': form}

    return render(request, 'slider-admin/addslider.html', context_dict)

@login_required
def edit_slider(request,id):
	if request.method == 'POST':
		slider = Slider.objects.get(id=id)
		form = CategoryForm(request.POST, request.FILES, instance=slider)
		if form.is_valid():
			form.save()
			messages.info(request,"Slider edited successfully.")
		else:
			messages.error(request, "Something is Wrong!")
		return HttpResponseRedirect('/dashboard/slider/')
	else:
		slider = Slider.objects.get(id=id)
		form = SliderForm(instance=slider)
		context_dict = {
			'form': form,
		}
		return render(request, 'slider-admin/editslider.html', context_dict)

@login_required
def delete_slider(request, id):
	slider = Slider.objects.get(id=id)
	try:
		slider.delete()
		messages.info(request, "Deleted Successfully!")
	except:
		messages.error(request, "Something went wrong!")
	return HttpResponseRedirect('/dashboard/slider/')
