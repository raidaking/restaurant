from django.urls import path
from catalogs import views

urlpatterns = [
    path('',views.home,name='home'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('404/',views.not_found, name='not_found'),
    # User side
    path('food/<id>/',views.load_food, name='load_food'),
    path('error/',views.load_error, name='load_error'),
    path('category/<name>/',views.load_food_category, name="load_food_category"),
    # Admin Side for food
    path('dashboard/food/',views.dashboard_food, name='dashboard_food'),
    path('dashboard/add/food/',views.add_food, name='add_food'),
    path('dashboard/edit/food/<id>/',views.edit_food, name='edit_food'),
    path('dashboard/del/food/<id>/',views.delete_food, name='delete_food'),
    #Admin side for category
    path('dashboard/categories/', views.category, name='category'),
    path('dashboard/add/cat/', views.add_category, name='add_category'),
    path('dashboard/edit/cat/<id>/', views.edit_category, name='edit_category'),
    path('dashboard/del/cat/<id>/', views.delete_category, name='delete_category'),

    # Specific restaurant
    path('restaurant/dashboard/food/',views.load_my_food, name='load_my_food'),
    path('restaurant/dashboard/add/food/', views.add_my_food, name='add_my_food'),
    path('restaurant/dashboard/edit/food/<id>/', views.edit_my_food, name='edit_my_food'),

    # slider 
    #Admin side for Sliders
    path('dashboard/slider/', views.dashboard_slider, name='dashboard_slider'),
    path('dashboard/slider/add/sli/', views.add_slider, name='add_slider'),
    path('dashboard/slider/edit/<id>/', views.edit_slider, name='edit_slider'),
    path('dashboard/slider/del/<id>/', views.delete_slider, name='delete_slider'),

]