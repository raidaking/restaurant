from django.db.models.signals import post_save
from django.dispatch import receiver
from restaurant.models import Restaurant, AboutRestaurant
from restaurant.views import email_user

@receiver(post_save, sender=Restaurant)
def restaurant_handler(sender, instance, created,**kwargs):
    if created:
        AboutRestaurant.objects.create(restaurant=instance)
        email_user("Registration of restaurant",
                   "Your restaurant is accepted!",
                   instance.email)