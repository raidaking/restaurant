from django.db import models
from django.utils.text import slugify
from django.conf import settings

class Address(models.Model):
	"""
	Author: Daking Rai
	Date: June 12, 2018
	Description: Address model that is used by user and restaurant model.
	"""
	city = models.CharField(max_length=200, null=True, blank=True)
	country = models.CharField(max_length=200, null=True, blank=True)
	zip_code = models.CharField(max_length=200, null=True, blank= True)
	address1 = models.CharField(max_length=200, null=True, blank= True)
	address2 = models.CharField(max_length=200, null=True, blank= True)

	class Meta:
		db_table = 'addresses'
		verbose_name_plural = 'addresses'

	def __str__(self):
		return ('{}{}'.format(self.city,self.country))  


class Restaurant(models.Model):
	"""
	Author: Daking Rai
	Date: June 12, 2018
	Description: Model for restaurant.
	"""
	name = models.CharField(max_length=200)
	slug = models.SlugField(unique=True)
	description = models.TextField(null=True,blank=True)
	is_active = models.BooleanField(default=True)
	meta_keywords = models.CharField(max_length=255, help_text='Comma seperated keywords that is used for SEO',null=True,blank=True)
	meta_description = models.CharField(max_length=255, help_text='description of meta tags',null=True,blank=True)
	image = models.ImageField(upload_to='restaurant/%Y/%m/%d', null=True)
	cover_image = models.ImageField(upload_to='restaurant/cover-image/%Y/%m/%d',null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, blank=True, null = True)
	phone = models.IntegerField(blank=True, null=True)
	address = models.ForeignKey(Address,on_delete=models.CASCADE,blank=True,null=True)
	website = models.CharField(max_length=300,blank=True, null=True)
	email = models.EmailField(max_length=254,blank=True,null=True)

	class Meta:
		db_table = 'restaurants'
		ordering = ['-created_at']
		verbose_name_plural = 'restaurants'     

	def __str__(self):
		return ('%s'%self.name)     

	def save(self, **kwargs):
		slug_str = "%s %s" % (self.name, self.id)
		self.slug = slugify(self, slug_str)
		super(Restaurant, self).save(**kwargs)
		
	@models.permalink
	def get_absolute_url(self):
		return ('load_restaurant', [self.id,])

class AboutRestaurant(models.Model):
	"""
	Author: Daking Rai
	Date: June 22, 2018
	Description: Model for about restaurant
	"""
	restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE)
	facebook_url = models.CharField(max_length=700, null=True, blank=True)
	twitter_url = models.CharField(max_length=700, null=True, blank=True)
	instagram_url = models.CharField(max_length=700, null=True, blank=True)
	youtube_url = models.CharField(max_length=700, null=True, blank=True)
	google_map_url = models.CharField(max_length=700, null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	opening_time= models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
	closing_time = models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
	minimum_order = models.IntegerField(blank=True, null=True)

	class Meta:
		db_table = 'about_restaurant'
		ordering = ['-created_at']

	def __str__(self):
		return ('%s'%self.restaurant.name)

class RegisterRestaurant(models.Model):
	"""
	Author: Daking Rai
	Date: June 12, 2018
	Description: Model for keeping the request form filled by user for registration of
	restaurant.
	"""
	name = models.CharField(max_length=200)
	website = models.CharField(max_length=200,blank=True,null=True)
	email = models.EmailField(max_length=200)
	address = models.CharField(max_length=200)
	phone = models.IntegerField()
	description = models.CharField(max_length=5000)
	is_approved = models.BooleanField(default=False)
	is_reviewed = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

	class Meta:
		db_table = 'restaurant_registration'
		ordering = ['-created_at']
		verbose_name_plural = 'restaurant_registrations'

	def __str__(self):
		return ('%s'%self.name)

