from django.urls import path
from restaurant import views

urlpatterns = [
    path('restaurant/<id>/', views.load_restaurant, name='load_restaurant'),
    path('restaurants/', views.load_all_restaurant, name='load_all_restaurant'),

    path('dashboard/restaurant/', views.dashboard_restaurant, name='dashboard_restaurant'),
    path('dashboard/add/restaurant/', views.add_restaurant, name='add_restaurant'),
    path('dashboard/edit/restaurant/<id>/', views.edit_restaurant, name='edit_restaurant'),
    path('dashboard/restaurant/about/edit/<id>',
         views.edit_about_restaurant, name='edit_about_restaurant'),
    path('dashboard/del/restaurant/<id>/', views.delete_restaurant, name='delete_restaurant'),
    path('dashboard/view-feedbacks/', views.view_feedback, name='view_feedback'),
    path('dashboard/view-customers/', views.view_customers, name='view_customers'),

    path('register/restaurant/', views.register_restaurant, name='register_restaurant'),
    path('review/register/restaurant/', views.review_restaurant, name='review_restaurant'),
    path('approve/register/restaurant/<id>/', views.approve_restaurant, name='approve_restaurant'),
    path('reject/register/restaurant/<id>/', views.reject_restaurant, name='reject_restaurant'),

    path('my/restaurant/', views.load_my_restaurant, name='load_my_restaurant'),
    path('my/restaurant/edit/', views.edit_my_restaurant, name='edit_my_restaurant'),
    path('my/restaurant/address/edit/<id>/',
         views.edit_my_restaurant_address, name='edit_my_restaurant_address'),
    path('my/restaurant/detail/edit/<id>/',
         views.edit_my_restaurant_detail, name='edit_my_restaurant_detail'),


]
