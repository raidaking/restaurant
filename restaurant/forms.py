from django import forms
from restaurant.models import Restaurant, Address, AboutRestaurant

# Form for requesting registration of restaurant.
class AddRestaurantForm(forms.Form):
	name = forms.CharField(max_length=100)
	website = forms.CharField(max_length=500)
	address = forms.CharField(max_length=100)
	phone = forms.IntegerField()
	email = forms.EmailField(max_length=100)
	description = forms.CharField(max_length=5000)

class AddressForm(forms.ModelForm):
	class Meta:
		model = Address
		fields = '__all__'

class RestaurantModelForm(forms.ModelForm):
	class Meta:
		model = Restaurant
		fields = '__all__'
		exclude = ['slug','created_at','updated_at','address']

class AboutRestaurantForm(forms.ModelForm):
	class Meta:
		model = AboutRestaurant
		fields = '__all__'
		exclude = ['created_at','restaurant']