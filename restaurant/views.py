from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import Group

from catalogs.models import Food, Category
from restaurant.models import RegisterRestaurant, Restaurant, Address, AboutRestaurant
from accounts.models import RestaurantOwner, User
from shopping.models import Order, OrderDetail
from reviews.models import Feedback
from restaurant.forms import AddRestaurantForm, AddressForm, RestaurantModelForm, AboutRestaurantForm


def website_user(user):
    return user.groups.filter(name='website').exists()


def email_user(subject, body, to_email, from_email=settings.EMAIL_HOST_USER):
    """ Common Function for emailing user """
    send_mail(subject, body, from_email, [to_email])


def load_restaurant(request, id):
    """View function to display food menu for specific restaurant"""
    foods = Food.objects.filter(restaurant=id)
    restaurant = get_object_or_404(Restaurant, id=id)
    categories = Category.objects.filter(food__restaurant_id=id).distinct()
    about = get_object_or_404(AboutRestaurant, restaurant=restaurant.id)

    context_dict = {
        'foods': foods,
        'restaurant': restaurant,
        'categories': categories,
        'about': about,

    }

    return render(request, 'restaurant/restaurant.html', context_dict)


def load_all_restaurant(request):
    restaurants = Restaurant.objects.all().order_by('created_at')[:9]
    return render(request, 'restaurant/allrestaurant.html', {'restaurants': restaurants})

# Views function for admin side


@login_required
@user_passes_test(website_user, login_url='/404')
def dashboard_restaurant(request):
    restaurants = Restaurant.objects.all()
    context_dict = {
        'restaurants': restaurants,
    }
    return render(request, 'restaurant-admin/restaurant.html', context_dict)


@login_required
@user_passes_test(website_user, login_url='/404')
def add_restaurant(request):
    if request.method == 'POST':
        restaurant_form = RestaurantModelForm(request.POST, request.FILES)
        address_form = AddressForm(request.POST)
        if restaurant_form.is_valid() and address_form.is_valid():
            a = address_form.save()

            Restaurant.objects.create(name=restaurant_form.cleaned_data['name'],
                                      description=restaurant_form.cleaned_data['description'],
                                      image=restaurant_form.cleaned_data['image'],
                                      cover_image=restaurant_form.cleaned_data['cover_image'],
                                      phone=restaurant_form.cleaned_data['phone'],
                                      address=a, email=restaurant_form.cleaned_data['email'],
                                      website=restaurant_form.cleaned_data['website'])

            messages.info(request, 'Registration Success!')
        else:
            messages.error(request, 'Something is Wrong, Try Again!')

    restaurant_form = RestaurantModelForm()
    address_form = AddressForm()

    context_dict = {
        'address_form': address_form,
        'restaurant_form': restaurant_form,
        'about_form': AboutRestaurantForm,
    }

    return render(request, 'restaurant-admin/addrestaurant.html', context_dict)


@login_required
@user_passes_test(website_user, login_url='/404')
def edit_restaurant(request, id):
    if request.method == 'POST':
        restaurant = Restaurant.objects.get(id=id)
        form = RestaurantModelForm(request.POST, request.FILES, instance=restaurant)
        address = Address.objects.get(id=restaurant.address.id)
        address_form = AddressForm(request.POST, instance=address)
        if form.is_valid() and address_form.is_valid():
            form.save()
            address_form.save()
            messages.info(request, 'Restaurant edit successful')
        else:
            messages.error(request, 'Something is Wrong, Try Again!')
        return HttpResponseRedirect('/dashboard/restaurant/')
    else:
        restaurant = Restaurant.objects.get(id=id)

        # Checks if the restaurant has address. If it is none, then it creates one and assigns to restaurant.
        if not restaurant.address:
            address = Address.objects.create(city='city', zip_code='zip_code', country='country',
                                             address1='Address 1', address2='Address 2')
            restaurant.address = address
            restaurant.save()
        else:
            address = get_object_or_404(Address, id=restaurant.address.id)

        about = get_object_or_404(AboutRestaurant, restaurant=id)

        context_dict = {
            'restaurant': restaurant,
            'address': address,
            'about': about,


        }
        return render(request, 'restaurant-admin/editrestaurant.html', context_dict)


@login_required
@user_passes_test(website_user, login_url='/404')
def edit_about_restaurant(request, id):
    if request.method == 'POST':
        about = AboutRestaurant.objects.get(id=id)
        form = AboutRestaurantForm(request.POST, instance=about)
        if form.is_valid():
            form.save()
            messages.info(request, "Saved Successfully")
        else:
            messages.error(request, "Something is wrong")
        return HttpResponseRedirect('/dashboard/restaurant/')


@login_required
@user_passes_test(website_user, login_url='/404')
def delete_restaurant(request, id):
    try:
        restaurant = get_object_or_404(Restaurant, id=id)
        restaurant.delete()
        messages.info(request, "Deleted Successfully!")
    except:
        messages.error(request, "Something went Wrong, try again!")

    return HttpResponseRedirect('/dashboard/restaurant/')


@login_required
@user_passes_test(website_user, login_url='/404')
def view_feedback(request):
    feedbacks = Feedback.objects.all()
    context_dict = {
        'feedbacks': feedbacks,
    }
    return render(request, 'feedback/feedback.html', context_dict)


# View function for registration of restaurant


def register_restaurant(request):
    """
    View function to handle registration of restaurant
    """

    if request.method == 'POST':
        if not request.user.is_authenticated:
            messages.error(request, 'You must be logged in to register restaurant!')
            return HttpResponseRedirect('/accounts/login/')
        else:
            form = AddRestaurantForm(request.POST)
            if form.is_valid():
                RegisterRestaurant.objects.create(name=form.cleaned_data['name'],
                                                  description=form.cleaned_data['description'],
                                                  website=form.cleaned_data['website'],
                                                  phone=form.cleaned_data['phone'],
                                                  address=form.cleaned_data['address'],
                                                  email=form.cleaned_data['email'],
                                                  user=request.user)

                email_user("Registration of restaurant",
                           "Your restaurant is under review!",
                           form.cleaned_data['email'])

                messages.info(request, 'Registration Success!')
                return HttpResponseRedirect('/')

            else:
                messages.error(request, 'Something is Wrong')
                return render(request, 'restaurant/add_restaurant.html')
    else:
        return render(request, 'restaurant/register_restaurant.html')


@login_required
@user_passes_test(website_user, login_url='/404')
def review_restaurant(request):
    restaurants = RegisterRestaurant.objects.filter(is_reviewed=False)
    context_dict = {
        'restaurants': restaurants
    }
    return render(request, 'restaurant-admin/reviewrestaurant.html', context_dict)


@login_required
@user_passes_test(website_user, login_url='/404')
def view_customers(request):
    customers = User.objects.all()
    context_dict = {
        'customers': customers,
    }
    return render(request, 'restaurant/view-customers.html', context_dict)


@login_required
@user_passes_test(website_user, login_url='/404')
def approve_restaurant(request, id):
    """
    This views function provides functionality to website staff/manager to approve restaurant registration
    request provided by user. Here, we set is_reviewed and is_approved to true. We copy information from
    restaurant-registration table to restaurant table and we set the user that has requested the registration
    to be manager.
    :param id: Restaurant id website_owner wants to approve
    :return: renders the admin panel restaurant template
    """
    register_restaurant = RegisterRestaurant.objects.get(id=id)
    register_restaurant.is_reviewed = True
    register_restaurant.is_approved = True
    register_restaurant.save()
    try:
        address = Address.objects.create()
        restaurant = Restaurant.objects.create(name=register_restaurant.name,
                                               phone=register_restaurant.phone,
                                               email=register_restaurant.email,
                                               website=register_restaurant.website,
                                               address=address)

        RestaurantOwner.objects.create(user=register_restaurant.user,
                                       is_manager=True,
                                       restaurant=restaurant)

        user = User.objects.get(id=register_restaurant.user.id)
        User.objects.filter(id=user.id).update(is_restaurant_staff=True)

        restaurant_group = Group.objects.get(name='restaurant')
        restaurant_group.user_set.add(user)
        messages.info(request, "Restaurant approval success!")
    except:
        messages.error(request, "Something is Wrong!")

    return HttpResponseRedirect('/dashboard/restaurant/')


@login_required
@user_passes_test(website_user, login_url='/404')
def reject_restaurant(request, id):
    """
    Description: This views function allow website staff/manager to reject the
    restaurant registration request provided by user.
    :param id: Id of restaurant that you want to delete
    :return: template of review restaurant
    """
    restaurant = RegisterRestaurant.objects.get(id=id)
    restaurant.is_reviewed = True
    restaurant.save()
    email_user("Registration of restaurant", "Your restaurant is declined!", restaurant.email)
    return HttpResponseRedirect('/review/register/restaurant/')


# View functions for restaurant owner backend
@login_required
def load_my_restaurant(request):
    restaurant = RestaurantOwner.objects.get(user=request.user)
    orders = OrderDetail.objects.filter(food__restaurant_id=restaurant.id)
    order_on_process = OrderDetail.objects.filter(food__restaurant_id=restaurant.id,
                                                  status='ON_PROCESS').count()
    order_recieved = OrderDetail.objects.filter(food__restaurant_id=restaurant.id,
                                                status='ORDER_RECEIVED').count()
    order_delivered = OrderDetail.objects.filter(food__restaurant_id=restaurant.id,
                                                 status='DELIVERED').count()

    context_dict = {
        'orders': orders,
        # 'order_details': order_details,
        'order_on_process': order_on_process,
        'order_recieved': order_recieved,
        'order_delivered': order_delivered,
    }
    return render(request, 'restaurant-owner/restaurant-dashboard.html', context_dict)


@login_required
def edit_my_restaurant(request):
    restaurant_owner = get_object_or_404(RestaurantOwner, user=request.user)
    restaurant = get_object_or_404(Restaurant, id=restaurant_owner.restaurant.id)
    restaurant_detail = AboutRestaurant.objects.get(restaurant=restaurant)
    if request.method == 'POST':
        form = RestaurantModelForm(request.POST, request.FILES, instance=restaurant)
        if form.is_valid():
            form.save()
            messages.info(request, 'Restaurant edit successful')
        else:
            messages.error(request, 'Something is Wrong, Try Again!')
        return HttpResponseRedirect('/my/restaurant/edit')

    else:
        context_dict = {
            'restaurant': restaurant,
            'restaurant_detail': restaurant_detail,
        }
        return render(request, 'restaurant-owner/editrestaurant.html', context_dict)


@login_required
def edit_my_restaurant_address(request, id):
    if request.method == 'POST':
        address = get_object_or_404(Address, id=id)
        form = AddressForm(request.POST, instance=address)
        if form.is_valid():
            form.save()
            messages.info(request, 'Restaurant Address edit successful')
        else:
            messages.error(request, 'Something is wrong, try again!')

    return HttpResponseRedirect('/my/restaurant/edit/')


@login_required
def edit_my_restaurant_detail(request, id):
    if request.method == 'POST':
        about_restaurant = AboutRestaurant.objects.get(id=id)
        if about_restaurant:
            form = AboutRestaurantForm(request.POST, instance=about_restaurant)
            if form.is_valid():
                form.save()
                messages.info(request, 'Restaurant Social Links edit successful')
            else:
                messages.error(request, 'Something is wrong, try again!')

        else:
            form = AboutRestaurantForm(request.POST)
            if form.is_valid():
                restaurant_owner = get_object_or_404(RestaurantOwner, user=request.user)
                restaurant = get_object_or_404(Restaurant, id=restaurant_owner.restaurant.id)
                restaurant_detail = form.save(commit=False)
                restaurant_detail.restaurant = restaurant
                form.save()
                messages.info(request, 'Restaurant Social Links edit successful')
            else:
                messages.error(request, 'Something is wrong, try again!')

    return HttpResponseRedirect('/my/restaurant/edit/')
