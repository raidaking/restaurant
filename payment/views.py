from datetime import datetime
from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received
import pdb
from django.contrib.auth.decorators import login_required

from django.urls import reverse
from django.shortcuts import render, redirect
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from carton.cart import Cart
from shopping.models import Order, OrderDetail
from catalogs.models import Food
from restaurant.forms import AddressForm
from shopping.forms import CheckOutForm


# this is the view for paypal notification
def show_me_the_money(sender, **kwargs):
    pdb.set_trace()
    ipn_obj = sender
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        # Undertake some action depending upon `ipn_obj`.
        print('success')
    else:
        print('fail')


def payment_process(request):
    cart = Cart(request.session)
    items = []
    print(request.session.get('delivery_at'))
    for item in cart.items:
        items.append(str(item.product.name))

    # Creating a dictionary to sent it to Paypal API.
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "amount": str(cart.total),
        "item_name": ', '.join(items),
        "invoice": "unique-invoice-id",
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return": request.build_absolute_uri(reverse('payment:done')),
        "cancel_return": request.build_absolute_uri(reverse('payment:canceled')),
        "custom": "premium_plan",  # Custom command to correlate to some function later (optional)
    }

    # Create the instance.
    paypal_form = PayPalPaymentsForm(initial=paypal_dict)
    context_dict = {
        'paypal_form': paypal_form,
    }
    return render(request, 'payment/process.html', context_dict)


@csrf_exempt
def payment_done(request):
    cart = Cart(request.session)

    full_addr = [request.session.get('city'),
                 request.session.get('country'),
                 request.session.get('address1'),
                 request.session.get('address2')]

    # Converting the input date to django standard date.
    order_date = request.session.get('delivery_date')
    order_time = request.session.get('delivery_time')

    # Creating a context dictionary with user information
    context_dict = {
        'city': request.session.get('city'),
        'country': request.session.get('country'),
        'address1': request.session.get('address1'),
        'address2': request.session.get('address2'),
        'items': cart.items,
    }

    # saving the order to database
    order_instance = Order.objects.create(delivery_date=order_date,
                                          delivery_time=order_time,
                                          user=request.user,
                                          address=', '.join(full_addr),
                                          phone=request.session.get('phone'),
                                          remarks=request.session.get('remarks'))
    order_instance.save()

    # Creating order details for every orders made.
    for item in cart.items:
        # Getting food detail of order items
        food = Food.objects.get(pk=item.product.id)

        # Saving Order detail to database.
        OrderDetail.objects.create(food=food,
                                   order=order_instance,
                                   quantity=item.quantity,
                                   extra_info=item.extra_info)

    # deleting all session value except login/user detail
    for key in list(request.session.keys()):
        # skip keys set by the django system
        if not key.startswith("_"):
            del request.session[key]

    return render(request, 'payment/done.html', context_dict)

# this is the return url view of canceled payment


@csrf_exempt
def payment_canceled(request):
    return render(request, 'payment/canceled.html')


# calling function for response from paypal API
valid_ipn_received.connect(show_me_the_money)

invalid_ipn_received.connect(show_me_the_money)
