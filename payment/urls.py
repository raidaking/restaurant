from django.urls import path, include
from . import views

# defining app name for payment
app_name = 'payment'

urlpatterns = [

    path('payment-process/', views.payment_process, name='payment_process'),
    path('done/', views.payment_done, name='done'),
    path('canceled/', views.payment_canceled, name='canceled'),
]
