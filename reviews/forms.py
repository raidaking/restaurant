from django import forms
from reviews.models import Feedback, Comment

# Form for feedback form to user
class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'
        exclude = ['created_at','user']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['food','comment']