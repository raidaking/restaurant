from django.db import models
from catalogs.models import Food

from django.conf import settings

class Comment(models.Model):
	"""
	Author: Daking Rai
	Date: June 15, 2018
	Description: Model for comment to food.
	"""
	comment = models.CharField(max_length=1000)
	food = models.ForeignKey(Food, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True,blank=True, null = True)
	sentiment = models.CharField(max_length=6, blank=True, null = True)
	user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

	class Meta:
		db_table = 'comments'
		ordering = ['-created_at']
		verbose_name_plural = 'comments'

	def __repr__(self):
		return '<%s: %s>' % (self.__class__.__name__, self)

class Rating(models.Model):
	"""
	Author: Daking Rai
	Date: June 15, 2018
	Description: Model for rating to food.
	"""
	rating = models.IntegerField()
	food = models.ForeignKey(Food, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True,blank=True, null = True)
	user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

	class Meta:
		db_table = 'ratings'
		ordering = ['-created_at']
		verbose_name_plural = 'ratings'

	def __str__(self):
		return ('%s'%self.food)

class Feedback(models.Model):
	"""
	Author: Daking Rai
	Date: June 19, 2018
	Description: Model for collecting feedback from user.
	"""
	name = models.CharField(max_length=200)
	email = models.EmailField()
	title = models.CharField(max_length=500, null=True, blank=True)
	feedback = models.CharField(max_length=16384)
	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
	)

	class Meta:
		db_table = 'feedback'
		ordering = ['-created_at']
		verbose_name_plural = 'feedbacks'

	def __str__(self):
		return ('{},{}'.format(email,title))
