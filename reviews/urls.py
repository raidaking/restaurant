from django.urls import path
from reviews import views

urlpatterns = [
    path('comment/add/',views.add_comment, name="add_comment"),
    path('submit/feedback/',views.submit_feedback, name="submit_feedback"),
]