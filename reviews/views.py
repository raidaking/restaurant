from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages

from reviews.models import Comment, Feedback
from reviews.forms import FeedbackForm, CommentForm

def add_comment(request):
    if not request.user.is_authenticated:
        messages.error(request, 'You must be logged in to comment or leave your review!')
        return HttpResponseRedirect('/accounts/login/')
    else:
        form = CommentForm(request.POST)
        if form.is_valid():
            print("iam here")
            print(request.user.id)
            Comment.objects.create(comment=form.cleaned_data['comment'], food=form.cleaned_data['food'],\
                                   user=request.user)
        else:
            messages.error(request,"Something went Wrong! Please Try again!!")

        return HttpResponseRedirect("/food/{}".format(request.POST['food']))

def submit_feedback(request):
    """View function that handles the feedback submitted from home page"""
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            if not request.user.is_authenticated:
                form.save()
            else:
                Feedback.objects.create(name=form.cleaned_data['name'], \
                                        email=form.cleaned_data['email'],\
                                        feedback=form.cleaned_data['feedback'],\
                                        user = request.user
                                        )
            messages.info(request, 'Your Feedback has been submitted!')

        else:
            messages.error(request, 'Something is wrong!')

        return HttpResponseRedirect("/")
