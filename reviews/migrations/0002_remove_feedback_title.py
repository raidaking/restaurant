# Generated by Django 2.0.7 on 2018-07-17 07:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feedback',
            name='title',
        ),
    ]
