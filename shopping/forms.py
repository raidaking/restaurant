import datetime
from django import forms
from tempus_dominus.widgets import DatePicker, TimePicker

# Form for showing and getting date from user


class CheckOutForm(forms.Form):
    date_field = forms.DateField(
        required=True,
        widget=DatePicker(
            options={
                'minDate': datetime.date.today().strftime('%Y-%m-%d'),


            }
        ),
    )
    time_field = forms.TimeField(
        required=True,
        widget=TimePicker(

        ),
    )
