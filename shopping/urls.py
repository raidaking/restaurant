from django.urls import path
from shopping import views

urlpatterns = [
    path('add/cart/',views.add_cart,name='addcart'),
    path('cart/',views.show_cart, name='showcart'),
    path('remove/<id>',views.remove_cart,name='removecart'),
    path('edit',views.edit,name='editcart'),
    
    path('order_detail/<id>/', views.order_detail, name='order_detail'),
    path('my/restaurant/<id>/', views.edit_order_status, name='edit_food_order_status'),
    
]