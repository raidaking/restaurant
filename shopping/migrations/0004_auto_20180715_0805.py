# Generated by Django 2.0.7 on 2018-07-15 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopping', '0003_auto_20180715_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('ON_DELIVERY', 'ON_DELIVERY'), ('OREDER_RECEIVED', 'ORDER_RECEIVED'), ('DELIVERED', 'DELIVERED'), ('ON_PROCESS', 'ON_PROCESS')], default='ORDER_RECEIVED', max_length=50),
        ),
        migrations.AlterField(
            model_name='orderdetail',
            name='status',
            field=models.CharField(choices=[('ON_PROCESS', 'ON_PROCESS'), ('OREDER_RECEIVED', 'ORDER_RECEIVED'), ('DELIVERED', 'DELIVERED')], default='ORDER_RECEIVED', max_length=50),
        ),
    ]
