import uuid

from django.db import models
from django.conf import settings
from django.utils import timezone

from catalogs.models import Food


class Order(models.Model):

    """
    Author: Rupesh Dahal
    Date: June 29, 2018
    Description: Model for saving orders to database.
    """

    STATUS_CHOICES = {
        ('OREDER_RECEIVED', 'ORDER_RECEIVED'),
        ('ON_PROCESS', 'ON_PROCESS'),
        ('DELIVERED', 'DELIVERED'),
        ('ON_DELIVERY', 'ON_DELIVERY'),
    }

    order_id = models.UUIDField(default=uuid.uuid4, editable=False)
    ordered_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='ORDER_RECEIVED')
    delivery_date = models.DateField(blank=True, null=True)
    delivery_time = models.TimeField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        default=None
    )
    address = models.CharField(max_length=300, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)
    remarks = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'order'

    # Defining function to get all status choices
    def status_verbose(self):
        return dict(Order.STATUS_CHOICES)

    def __str__(self):
        return str(self.user.username) + ' ' + str(self.status)


class OrderDetail(models.Model):
    """
            Author: Rupesh Dahal
            Date: June 29, 2018
            Description: Model for saving orders details to database.
    """
    STATUS_CHOICES = {
        ('OREDER_RECEIVED', 'ORDER_RECEIVED'),
        ('ON_PROCESS', 'ON_PROCESS'),
        ('DELIVERED', 'DELIVERED'),
    }

    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    extra_info = models.TextField()
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='ORDER_RECEIVED')

    class Meta:
        db_table = 'order_detail'

    def status_verbose(self):
        return dict(OrderDetail.STATUS_CHOICES)

    def __str__(self):
        return str(self.food) + ' ' + str(self.quantity)
