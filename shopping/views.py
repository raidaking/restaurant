from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from carton.cart import Cart
from catalogs.models import Food
from shopping.models import Order, OrderDetail
from restaurant.forms import AddressForm
from shopping.forms import CheckOutForm


def add_cart(request):
    cart = Cart(request.session)
    id = request.GET.get('id')
    quantity = request.GET.get('quantity')
    extra_info = request.GET.get('extra_info')
    if int(quantity) > 0:
        food = Food.objects.get(id=id)
        cart.add(food, price=food.price, quantity=quantity, extra_info=extra_info)
    else:
        messages.error(request, 'Quantity should be greater than 0')
    return HttpResponseRedirect('/food/{}'.format(int(id)))


def show_cart(request):
    if request.method == 'POST':
        form = AddressForm(request.POST)

        request.session['phone'] = request.POST.get('phone')
        request.session['remarks'] = request.POST.get('remarks')
        request.session['delivery_date'] = form.data['date_field']
        request.session['delivery_time'] = form.data['time_field']
        if form.is_valid():
            request.session['city'] = form.cleaned_data['city']
            request.session['country'] = form.cleaned_data['country']
            request.session['zip_code'] = form.cleaned_data['zip_code']
            request.session['address1'] = form.cleaned_data['address1']
            request.session['address2'] = form.cleaned_data['address2']

            return redirect("/payment-process/")
    else:

        form = CheckOutForm()
        if not request.user.is_authenticated:
            return render(request, 'payment/payment.html')
        else:
            address = request.user.address
            context_dict = {
                'address': address,
                'form': form,
            }
            return render(request, 'mycart.html', context_dict)

# View function to remove item from cart.


def remove_cart(request, id):
    cart = Cart(request.session)
    food = Food.objects.get(id=id)
    cart.remove(food)
    return HttpResponseRedirect('/cart/')

# View function to edit quantity in cart.


def edit(request):
    cart = Cart(request.session)
    id = request.GET.get('id')
    quantity = request.GET.get('quantity')
    if int(quantity) > 0:
        food = Food.objects.get(id=id)
        cart.remove(food)  # remove food from cart; otherwise it will add the quantity
        cart.add(food, price=food.price, quantity=quantity)
    else:
        messages.error(request, 'Quantity should be greater than 0')
    return HttpResponseRedirect('/cart/')


# detail of orders in the dashboard
@login_required
@csrf_exempt
def order_detail(request, id):
    orders = Order.objects.filter(user=request.user)
    order_details = OrderDetail.objects.filter(order=id)

    # requesting users input for choices.
    if request.method == 'POST':
        order = Order.objects.get(id=id)
        order.status = request.POST.get('status')
        order.save(update_fields=['status'])
        return HttpResponseRedirect('/dashboard/')

    context_dict = {
        'orders': orders,
        'order_details': order_details,
    }

    return render(request, 'order-admin/order-detail.html', context_dict)


def edit_order_status(request, id):
    # requesting users input for choices.
    if request.method == 'POST':
        order = OrderDetail.objects.get(id=id)
        order.status = request.POST.get('status')
        order.save(update_fields=['status'])
        return HttpResponseRedirect('/my/restaurant/')
