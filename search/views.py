from django.shortcuts import render
from django.contrib.postgres.search import SearchVector, SearchRank, SearchQuery

from restaurant.models import Restaurant
from catalogs.models import Food
from search.spellcorrect import SpellCorrection
from search.Mytokenizer import Tokenizer

from textblob import TextBlob

def search_view(request):
    """
    Author: Daking Rai
    Date: Thursday, May 25
    Description: search_view function handles the required preprocessing before querying database like pos tagging,
    word spelling autocorrection.
    """

    if request.method == 'GET':

        # Stores the columns of database where query searches for match.
        vector = SearchVector('name') + SearchVector('restaurant__name') + \
        SearchVector('restaurant__address__city') + \
        SearchVector('restaurant__address__address1') + SearchVector('description')

        pos_tagged = get_pos(request.GET.get('q'))
        nouns,preposition,number,conjuction = parse_pos(pos_tagged)

        # corrects spelling of nouns
        noun_spell = SpellCorrection(nouns)
        nouns = noun_spell.correct()

        feature = get_feature(pos_tagged)

        # creates query using noun as topic for searching in database
        try:
            query = ''
            result = []

            if feature['nouns']>0:
                for noun in nouns:
                    if noun == nouns[-1]:

                        query += "SearchQuery('{}')".format(noun)
                    elif noun == 'restaurant':
                        pass
                        
                    else:
                        query += "SearchQuery('{}') & ".format(noun)

                # converting string query to SearchQuery
                query = eval(query)

                # Different conditions for different type of queries
            
                if feature['number']>0 and feature['has_above']>0:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query, price__gt=int(number[0]))

                elif feature['number']>0 and feature['has_lower']>0:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query, price__lt=int(number[0]))

                elif feature['number']>1 and feature['has_between']>0:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query, price__range=(number[0],number[1]))

                elif feature['has_cheap']>0 and feature['number']==0:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query).order_by('price')
                elif feature['number']>0:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query, price = number[0])
                else:
                    result = Food.objects.annotate(search = vector,
                                           ).filter(search=query)
        except:
                result = []
                print("caught exception in")
            

        args = {
            'result': result,
            'query': request.GET.get('q'),
        }

        return render(request, 'search/mysearch.html', args)

def restaurant_search(request):
    if request.method == 'GET':
        # Stores the columns of database where query searches for match.
        vector = SearchVector('name') + SearchVector('address__address2') + \
                 SearchVector('address__city') + SearchVector('address__country') +\
                 SearchVector('address__address1') + SearchVector('description')

        tokenizer = Tokenizer(request.GET.get('q'))
        tokens = tokenizer.tokenize()
        print(tokens)

        # creates query using noun as topic for searching in database
        try:
            query = ''
            result = []

            for token in tokens:
                    if token == tokens[-1]:

                        query += "SearchQuery('{}')".format(token)

                    else:
                        query += "SearchQuery('{}') & ".format(token)

            # converting string query to SearchQuery
            query = eval(query)

            result = Restaurant.objects.annotate(search=vector,
                                                   ).filter(search=query)
        except:
            result = []
            print("caught exception in")

        args = {
            'result': result,
            'query': request.GET.get('q'),
        }

        return render(request, 'search/mysearch.html', args)

def get_pos(text):
    """
    return: tuple of word and pos tagging of the word
    """
    blob = TextBlob(text)
    return blob.tags

def parse_pos(pos_tagged):
    """
    return: Tuple of nouns, preposition, number and conjuction seperated from pos tagging
    """
    nouns = [word for word,pos in pos_tagged if pos == 'NN' or pos =='NNS']
    preposition = [word for word,pos in pos_tagged if pos == 'IN']
    number = [word for word,pos in pos_tagged if pos == 'CD']
    conjuction = [word for word,pos in pos_tagged if pos == 'CC']

    return nouns,preposition,number,conjuction

def get_feature(pos_tagged):
    """
    return: Dictionary of features of the pos_tagged query entered by user like number of nouns,
    preposition,has_above and more. This is used to classify the type of query for getting search result.
    """
    return {
        'nouns': len([word for word,p in pos_tagged if p =='NN' or p =='NNS']),
        'preposition' : len([word for word,p in pos_tagged if p =='IN']),
        'number' : len([word for word,p in pos_tagged if p =='CD']),
        'conjunction' : len([word for word,p in pos_tagged if p =='CC']),
        'has_above': len([True for to,v in pos_tagged if to in ['high','higher','above','greater','<']]),
        'has_between': len([True for to,v in pos_tagged if to in ['interval','between','-']]),
        'has_lower': len([True for to,v in pos_tagged if to in ['below','under','lower','low','>', 'less','cheaper']]),
        'has_best': len([True for to,v in pos_tagged if to in ['best','good']]),
        'has_cheap': len([True for to,v in pos_tagged if to in ['cheap','cheapest','lowest','low','order','orderby']]),
        'has_around': len([True for to,v in pos_tagged if to in ['around','near','nearby','in']]),
            }