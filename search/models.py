from django.db import models
from django.conf import settings

# Create your models here.
class Search(models.Model):

	search = models.TextField(max_length=500)
	created_at = models.DateTimeField(auto_now_add = True)
	user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

	class Meta:
		db_table = 'search'
		ordering = ['-created_at']

	def __repr__(self):
		return '<%s: %s>' % (self.__class__.__name__, self)
