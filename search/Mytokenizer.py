import re

class Tokenizer:
    """
    Author: Daking Rai
    Date: Thursday, May 11
    Description: Tokenizer class is used to tokenize the text.
    """

    def __init__(self,text):
        self.text = text

    def remove_punctuations(self):
        return re.sub(r"[()-_.:;!,*?\s\n]+", " ", self.text)

    def tokenize(self):
        return re.split(' ', self.remove_punctuations())