from django.urls import path
from . import views

urlpatterns = [
    path('mysearch/', views.search_view),
    path('mysearch/restaurant/',views.restaurant_search),
]