import pickle
from search.Mytokenizer import Tokenizer
import os
from django.conf import settings

class SpellCorrection():
    """
    Author: Daking Rai
    Date: 5/14/2018
    Description: This class is used for correcting the spelling of word.
    """

    def __init__(self,tokens):
        self.path = settings.STATICFILES_DIRS[0]
        self.token = tokens

    def load_searchwords(self):
        with open(self.path+'/pickles/searchword.pickle', 'rb') as f:
            self.WORDS = pickle.load(f)

    def P(self, word):
        """Probability of word."""
        return self.WORDS[word] / sum(self.WORDS.values())

    def correction(self, word):
        """Most probable spelling correction for word."""
        return max(self.candidates(word), key=self.P)

    def candidates(self, word):
        """Generate possible spelling corrections for word."""
        return (self.known([word]) or self.known(self.edits1(word))
                or self.known(self.edits2(word)) or [word])

    def known(self, words):
        """The subset of `words` that appear in the dictionary of WORDS."""
        return set(w for w in words if w in self.WORDS)

    def edits1(self, word):
        """All edits that are one edit away from `word`."""
        letters = 'abcdefghijklmnopqrstuvwxyz'
        splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes = [L + R[1:] for L, R in splits if R]
        transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R) > 1]
        replaces = [L + c + R[1:] for L, R in splits if R for c in letters]
        inserts = [L + c + R for L, R in splits for c in letters]
        return set(deletes + transposes + replaces + inserts)

    def edits2(self, word):
        """All edits that are two edits away from `word`."""
        return (e2 for e1 in self.edits1(word) for e2 in self.edits1(e1))

    def correct(self):
        # self.load_token()
        self.load_searchwords()
        return [self.correction(w) for w in self.token]


